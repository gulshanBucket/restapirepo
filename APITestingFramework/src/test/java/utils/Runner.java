package utils;


import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = "src/test/java/utils/Screenshot.feature"
			,glue={"stepDefinitions","utils"}
	)


	public class Runner {

	}
