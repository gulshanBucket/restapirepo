Feature: Weather API
  Weather API Automation
  
  Background: User generates token for authorisation 
	Given I am an authorised user with a valid "3af0af8ee18347358e047dd11af0a63a" key
	
  @Current_Weather_API
  Scenario: Get weather by latitude and longitude
    Given I have "40.730610" and "-73.935242" of a given location
    When I hit the weatherbit application api with desired longitude and latitude
    Then I get the weather details with statecode as "NC" for the request
   
  @Weather_Forecast_API
 Scenario: Get three hourly weather forecast by postal code
    Given I have the postal code "29014" of a given location in "US"
    When I hit the weatherbit application api with desired postal code
    Then I get the weather details timestamp and weather for that location
