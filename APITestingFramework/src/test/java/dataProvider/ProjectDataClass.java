package dataProvider;
/**
 * This class will hold the data for Google Search test execution project.
 * It has getter and setter function only.
 */

public class ProjectDataClass {
	
	private final String BASE_URL = "https://api.weatherbit.io/";
	private String key = "3af0af8ee18347358e047dd11af0a63a";
	private String latitude = "40.730610";
	private String longitude = "-73.935242";
	private String postalCode = "29014";
	private String countryCode = "US";
	
	private static ProjectDataClass dataInstance = null;
	
	/**
	 * Method to get class object
	 * @return
	 */
	public static ProjectDataClass getInstance(){
		if(ProjectDataClass.dataInstance==null){
			dataInstance = new ProjectDataClass();
		}
		return dataInstance;
	}

	/**
	 * Method to set class object
	 * 
	 */
	public static void setInstance(ProjectDataClass dataInstance){
		ProjectDataClass.dataInstance = dataInstance;
	}
	
	public String getBaseURL() {
		return BASE_URL;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
}
