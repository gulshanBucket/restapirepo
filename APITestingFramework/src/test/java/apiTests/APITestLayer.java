package apiTests;

import org.junit.Assert;

import apiEngine.model.response.CurrentWeatherAndLocationData;
import apiEngine.model.response.ResponseCurrentWeatherData;
import apiEngine.model.response.ResponseWeatherForecast;
import apiEngine.model.response.WeatherForecastAndLocationData;


public class APITestLayer {
	
	/**
	 * Method to verify state code in current weather response API
	 * @param response
	 * @param stateCode
	 */
	public void verifyResponseStateCode(ResponseCurrentWeatherData response, String stateCode){

		
		for(CurrentWeatherAndLocationData responseData : response.getData()){
			System.out.println("State Code : " + responseData.getState_code());
			Assert.assertEquals("NY", responseData.getState_code());
		}
	}
	
	/**
	 * Method to get the timestamp_utc and Weather details for all data entries
	 */
	public void getTimeAndWeatherDetails(ResponseWeatherForecast response){
		
		for(WeatherForecastAndLocationData responseData : response.getData()){
			System.out.println("timestamp_utc : " + responseData.getTimestamp_utc());
			System.out.println("Weather.icon : " + responseData.getWeather().getIcon());
			System.out.println("Weather.code : " + responseData.getWeather().getCode());
			System.out.println("weather.description : " + responseData.getWeather().getDescription());
		}
		
	}
	

}
