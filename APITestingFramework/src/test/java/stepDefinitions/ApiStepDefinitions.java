package stepDefinitions;

import apiEngine.EndPoints;
import apiEngine.model.response.ResponseCurrentWeatherData;
import apiEngine.model.response.ResponseWeatherForecast;
import apiTests.APITestLayer;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProvider.ProjectDataClass;

public class ApiStepDefinitions {
	
	private EndPoints endPoints = null;
	private ResponseCurrentWeatherData responseCurrentWeather = null;
	private ResponseWeatherForecast responseWeatherForecast = null;
	private APITestLayer apiTest = null;
	
	public ApiStepDefinitions() {
		endPoints = new EndPoints();
		apiTest = new APITestLayer();
	}
	@Given("^I am an authorised user with a valid \"(.*?)\" key$")
	public void i_am_an_authorised_user(String key) throws Throwable {
		ProjectDataClass.getInstance().setKey(key);
	}

	@Given("^I have \"(.*?)\" and \"(.*?)\" of a given location$")
	public void i_have_and_of_a_given_location(String lat, String log) throws Throwable {
		ProjectDataClass.getInstance().setLatitude(lat);
		ProjectDataClass.getInstance().setLongitude(log);
	}

	@When("^I hit the weatherbit application api with desired longitude and latitude$")
	public void i_hit_the_weatherbit_application_api_with_desired_longitude_and_latitude() throws Throwable {
		responseCurrentWeather = endPoints.getCurrentWeatherDetail();
		
	}

	@Then("^I get the weather details with statecode as \"(.*?)\" for the request$")
	public void i_get_the_weather_details_for_that_location(String stateCode) throws Throwable {
		apiTest.verifyResponseStateCode(responseCurrentWeather, stateCode);
	}

	@Given("^I have the postal code \"(.*?)\" of a given location in \"(.*?)\"$")
	public void i_have_the_postal_code_of_a_given_location(String postalCode, String countryCode) throws Throwable {
		ProjectDataClass.getInstance().setPostalCode(postalCode);		
		ProjectDataClass.getInstance().setCountryCode(countryCode);
	}

	@When("^I hit the weatherbit application api with desired postal code$")
	public void i_hit_the_weatherbit_application_api_with_desired_postal_code() throws Throwable {
		responseWeatherForecast = endPoints.getWeatherForecast();
	}

	@Then("^I get the weather details timestamp and weather for that location$")
	public void i_get_the_weather_details_and_for_that_location() throws Throwable {
		apiTest.getTimeAndWeatherDetails(responseWeatherForecast);
	}

}
