package apiEngine.model.response;

public class WeatherBaseClass {
	protected String icon;
	protected String description;
	
	/**
	* No args constructor for use in serialization
	*
	*/
	public WeatherBaseClass() {
	}
	
	/**
	*
	* @param code
	* @param icon
	* @param description
	*/
	public WeatherBaseClass(String icon,  String description) {
	this.icon = icon;
	this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
