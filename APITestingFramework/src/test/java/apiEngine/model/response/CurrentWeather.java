package apiEngine.model.response;


	public class CurrentWeather extends WeatherBaseClass{
	
	private String code;
		
	/**
	* No args constructor for use in serialization
	*
	*/
	public CurrentWeather() {
	}
	
	/**
	*
	* @param code
	* @param icon
	* @param description
	*/
	public CurrentWeather(String icon, String code, String description) {
	this.icon = icon;
	this.code = code;
	this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}