package apiEngine.model.response;

public class WeatherForecastAndLocationData {
	
	private String wind_cdir;
	private Integer rh;
	private String pod;
	private String timestamp_utc;
	private Float pres;
	private Integer solar_rad;
	private Float ozone;
	private CurrentWeather weather;
	private Float wind_gust_spd;
	private String timestamp_local;
	private Integer snow_depth;
	private Integer clouds;
	private Integer ts;
	private Float wind_spd;
	private Integer pop;
	private String wind_cdir_full;
	private Float slp;
	private Integer dni;
	private Float dewpt;
	private Integer snow;
	private Integer uv;
	private Integer wind_dir;
	private Integer clouds_hi;
	private Integer precip;
	private Float vis;
	private Integer dhi;
	private Float app_temp;
	private String datetime;
	private Float temp;
	private Integer ghi;
	private Integer clouds_mid;
	private Integer clouds_low;

	/**
	* No args constructor for use in serialization
	*
	*/
	public WeatherForecastAndLocationData() {
	}

	/**
	*
	* @param pod
	* @param pres
	* @param wind_cdir
	* @param clouds
	* @param wind_spd
	* @param ozone
	* @param pop
	* @param datetime
	* @param timestamp_local
	* @param precip
	* @param timestamp_utc
	* @param weather
	* @param snow_depth
	* @param dni
	* @param clouds_mid
	* @param uv
	* @param vis
	* @param temp
	* @param dhi
	* @param clouds_hi
	* @param app_temp
	* @param ghi
	* @param dewpt
	* @param wind_dir
	* @param solar_rad
	* @param wind_gust_spd
	* @param clouds_low
	* @param rh
	* @param slp
	* @param snow
	* @param wind_cdir_full
	* @param ts
	*/
	public WeatherForecastAndLocationData(String wind_cdir, Integer rh, String pod, String timestamp_utc, Float pres, Integer solar_rad, Float ozone, CurrentWeather weather, Float wind_gust_spd, String timestamp_local, Integer snow_depth, Integer clouds, Integer ts, Float wind_spd, Integer pop, String wind_cdir_full, Float slp, Integer dni, Float dewpt, Integer snow, Integer uv, Integer wind_dir, Integer clouds_hi, Integer precip, Float vis, Integer dhi, Float app_temp, String datetime, Float temp, Integer ghi, Integer clouds_mid, Integer clouds_low) {
	this.wind_cdir = wind_cdir;
	this.rh = rh;
	this.pod = pod;
	this.timestamp_utc = timestamp_utc;
	this.pres = pres;
	this.solar_rad = solar_rad;
	this.ozone = ozone;
	this.weather = weather;
	this.wind_gust_spd = wind_gust_spd;
	this.timestamp_local = timestamp_local;
	this.snow_depth = snow_depth;
	this.clouds = clouds;
	this.ts = ts;
	this.wind_spd = wind_spd;
	this.pop = pop;
	this.wind_cdir_full = wind_cdir_full;
	this.slp = slp;
	this.dni = dni;
	this.dewpt = dewpt;
	this.snow = snow;
	this.uv = uv;
	this.wind_dir = wind_dir;
	this.clouds_hi = clouds_hi;
	this.precip = precip;
	this.vis = vis;
	this.dhi = dhi;
	this.app_temp = app_temp;
	this.datetime = datetime;
	this.temp = temp;
	this.ghi = ghi;
	this.clouds_mid = clouds_mid;
	this.clouds_low = clouds_low;
	}

	public String getWind_cdir() {
		return wind_cdir;
	}

	public void setWind_cdir(String wind_cdir) {
		this.wind_cdir = wind_cdir;
	}

	public Integer getRh() {
		return rh;
	}

	public void setRh(Integer rh) {
		this.rh = rh;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getTimestamp_utc() {
		return timestamp_utc;
	}

	public void setTimestamp_utc(String timestamp_utc) {
		this.timestamp_utc = timestamp_utc;
	}

	public Float getPres() {
		return pres;
	}

	public void setPres(Float pres) {
		this.pres = pres;
	}

	public Integer getSolar_rad() {
		return solar_rad;
	}

	public void setSolar_rad(Integer solar_rad) {
		this.solar_rad = solar_rad;
	}

	public Float getOzone() {
		return ozone;
	}

	public void setOzone(Float ozone) {
		this.ozone = ozone;
	}

	public CurrentWeather getWeather() {
		return weather;
	}

	public void setWeather(CurrentWeather weather) {
		this.weather = weather;
	}

	public Float getWind_gust_spd() {
		return wind_gust_spd;
	}

	public void setWind_gust_spd(Float wind_gust_spd) {
		this.wind_gust_spd = wind_gust_spd;
	}

	public String getTimestamp_local() {
		return timestamp_local;
	}

	public void setTimestamp_local(String timestamp_local) {
		this.timestamp_local = timestamp_local;
	}

	public Integer getSnow_depth() {
		return snow_depth;
	}

	public void setSnow_depth(Integer snow_depth) {
		this.snow_depth = snow_depth;
	}

	public Integer getClouds() {
		return clouds;
	}

	public void setClouds(Integer clouds) {
		this.clouds = clouds;
	}

	public Integer getTs() {
		return ts;
	}

	public void setTs(Integer ts) {
		this.ts = ts;
	}

	public Float getWind_spd() {
		return wind_spd;
	}

	public void setWind_spd(Float wind_spd) {
		this.wind_spd = wind_spd;
	}

	public Integer getPop() {
		return pop;
	}

	public void setPop(Integer pop) {
		this.pop = pop;
	}

	public String getWind_cdir_full() {
		return wind_cdir_full;
	}

	public void setWind_cdir_full(String wind_cdir_full) {
		this.wind_cdir_full = wind_cdir_full;
	}

	public Float getSlp() {
		return slp;
	}

	public void setSlp(Float slp) {
		this.slp = slp;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public Float getDewpt() {
		return dewpt;
	}

	public void setDewpt(Float dewpt) {
		this.dewpt = dewpt;
	}

	public Integer getSnow() {
		return snow;
	}

	public void setSnow(Integer snow) {
		this.snow = snow;
	}

	public Integer getUv() {
		return uv;
	}

	public void setUv(Integer uv) {
		this.uv = uv;
	}

	public Integer getWind_dir() {
		return wind_dir;
	}

	public void setWind_dir(Integer wind_dir) {
		this.wind_dir = wind_dir;
	}

	public Integer getClouds_hi() {
		return clouds_hi;
	}

	public void setClouds_hi(Integer clouds_hi) {
		this.clouds_hi = clouds_hi;
	}

	public Integer getPrecip() {
		return precip;
	}

	public void setPrecip(Integer precip) {
		this.precip = precip;
	}

	public Float getVis() {
		return vis;
	}

	public void setVis(Float vis) {
		this.vis = vis;
	}

	public Integer getDhi() {
		return dhi;
	}

	public void setDhi(Integer dhi) {
		this.dhi = dhi;
	}

	public Float getApp_temp() {
		return app_temp;
	}

	public void setApp_temp(Float app_temp) {
		this.app_temp = app_temp;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public Float getTemp() {
		return temp;
	}

	public void setTemp(Float temp) {
		this.temp = temp;
	}

	public Integer getGhi() {
		return ghi;
	}

	public void setGhi(Integer ghi) {
		this.ghi = ghi;
	}

	public Integer getClouds_mid() {
		return clouds_mid;
	}

	public void setClouds_mid(Integer clouds_mid) {
		this.clouds_mid = clouds_mid;
	}

	public Integer getClouds_low() {
		return clouds_low;
	}

	public void setClouds_low(Integer clouds_low) {
		this.clouds_low = clouds_low;
	}
	
	
}
