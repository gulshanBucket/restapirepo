package apiEngine.model.response;


	public class CurrentWeatherAndLocationData {
	
	private Integer rh;
	private String pod;
	private Float lon;
	private Float pres;
	private String timezone;
	private String ob_time;
	private String country_code;
	private Integer clouds;
	private Integer ts;
	private Integer solar_rad;
	private String state_code;
	private String city_name;
	private Float wind_spd;
	private String wind_cdir_full;
	private String wind_cdir;
	private Float slp;
	private Integer vis;
	private Integer h_angle;
	private String sunset;
	private Float dni;
	private Float dewpt;
	private Integer snow;
	private Float uv;
	private Integer precip;
	private Integer wind_dir;
	private String sunrise;
	private Float ghi;
	private Float dhi;
	private Integer aqi;
	private Float lat;
	private CurrentWeather weather;
	private String datetime;
	private Integer temp;
	private String station;
	private Float elev_angle;
	private Float app_temp;
	
	/**
	* No args constructor for use in serialization
	*
	*/
	public CurrentWeatherAndLocationData() {
	}
	
	/**
	*
	* @param wind_cdir
	* @param sunrise
	* @param pod
	* @param pres
	* @param timezone
	* @param solar_rad
	* @param lon
	* @param clouds
	* @param wind_dir
	* @param wind_spd
	* @param datetime
	* @param precip
	* @param city_name
	* @param country_code
	* @param weather
	* @param station
	* @param dni
	* @param lat
	* @param vis
	* @param uv
	* @param temp
	* @param dhi
	* @param ghi
	* @param dewpt
	* @param elev_angle
	* @param h_angle
	* @param rh
	* @param ob_time
	* @param slp
	* @param snow
	* @param sunset
	* @param aqi
	* @param app_temp
	* @param state_code
	* @param wind_cdir_full
	* @param ts
	*/
	public CurrentWeatherAndLocationData(Integer rh, String pod, Float lon, Float pres, String timezone, String ob_time, String country_code, Integer clouds, Integer ts, Integer solar_rad, String state_code, String city_name, Float wind_spd, String wind_cdir_full, String wind_cdir, Float slp, Integer vis, Integer h_angle, String sunset, Float dni, Float dewpt, Integer snow, Float uv, Integer precip, Integer wind_dir, String sunrise, Float ghi, Float dhi, Integer aqi, Float lat, CurrentWeather weather, String datetime, Integer temp, String station, Float elev_angle, Float app_temp) {
	this.rh = rh;
	this.pod = pod;
	this.lon = lon;
	this.pres = pres;
	this.timezone = timezone;
	this.ob_time = ob_time;
	this.country_code = country_code;
	this.clouds = clouds;
	this.ts = ts;
	this.solar_rad = solar_rad;
	this.state_code = state_code;
	this.city_name = city_name;
	this.wind_spd = wind_spd;
	this.wind_cdir_full = wind_cdir_full;
	this.wind_cdir = wind_cdir;
	this.slp = slp;
	this.vis = vis;
	this.h_angle = h_angle;
	this.sunset = sunset;
	this.dni = dni;
	this.dewpt = dewpt;
	this.snow = snow;
	this.uv = uv;
	this.precip = precip;
	this.wind_dir = wind_dir;
	this.sunrise = sunrise;
	this.ghi = ghi;
	this.dhi = dhi;
	this.aqi = aqi;
	this.lat = lat;
	this.weather = weather;
	this.datetime = datetime;
	this.temp = temp;
	this.station = station;
	this.elev_angle = elev_angle;
	this.app_temp = app_temp;
	}

	public Integer getRh() {
		return rh;
	}

	public void setRh(Integer rh) {
		this.rh = rh;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public Float getLon() {
		return lon;
	}

	public void setLon(Float lon) {
		this.lon = lon;
	}

	public Float getPres() {
		return pres;
	}

	public void setPres(Float pres) {
		this.pres = pres;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getOb_time() {
		return ob_time;
	}

	public void setOb_time(String ob_time) {
		this.ob_time = ob_time;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public Integer getClouds() {
		return clouds;
	}

	public void setClouds(Integer clouds) {
		this.clouds = clouds;
	}

	public Integer getTs() {
		return ts;
	}

	public void setTs(Integer ts) {
		this.ts = ts;
	}

	public Integer getSolar_rad() {
		return solar_rad;
	}

	public void setSolar_rad(Integer solar_rad) {
		this.solar_rad = solar_rad;
	}

	public String getState_code() {
		return state_code;
	}

	public void setState_code(String state_code) {
		this.state_code = state_code;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public Float getWind_spd() {
		return wind_spd;
	}

	public void setWind_spd(Float wind_spd) {
		this.wind_spd = wind_spd;
	}

	public String getWind_cdir_full() {
		return wind_cdir_full;
	}

	public void setWind_cdir_full(String wind_cdir_full) {
		this.wind_cdir_full = wind_cdir_full;
	}

	public String getWind_cdir() {
		return wind_cdir;
	}

	public void setWind_cdir(String wind_cdir) {
		this.wind_cdir = wind_cdir;
	}

	public Float getSlp() {
		return slp;
	}

	public void setSlp(Float slp) {
		this.slp = slp;
	}

	public Integer getVis() {
		return vis;
	}

	public void setVis(Integer vis) {
		this.vis = vis;
	}

	public Integer getH_angle() {
		return h_angle;
	}

	public void setH_angle(Integer h_angle) {
		this.h_angle = h_angle;
	}

	public String getSunset() {
		return sunset;
	}

	public void setSunset(String sunset) {
		this.sunset = sunset;
	}

	public Float getDni() {
		return dni;
	}

	public void setDni(Float dni) {
		this.dni = dni;
	}

	public Float getDewpt() {
		return dewpt;
	}

	public void setDewpt(Float dewpt) {
		this.dewpt = dewpt;
	}

	public Integer getSnow() {
		return snow;
	}

	public void setSnow(Integer snow) {
		this.snow = snow;
	}

	public Float getUv() {
		return uv;
	}

	public void setUv(Float uv) {
		this.uv = uv;
	}

	public Integer getPrecip() {
		return precip;
	}

	public void setPrecip(Integer precip) {
		this.precip = precip;
	}

	public Integer getWind_dir() {
		return wind_dir;
	}

	public void setWind_dir(Integer wind_dir) {
		this.wind_dir = wind_dir;
	}

	public String getSunrise() {
		return sunrise;
	}

	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}

	public Float getGhi() {
		return ghi;
	}

	public void setGhi(Float ghi) {
		this.ghi = ghi;
	}

	public Float getDhi() {
		return dhi;
	}

	public void setDhi(Float dhi) {
		this.dhi = dhi;
	}

	public Integer getAqi() {
		return aqi;
	}

	public void setAqi(Integer aqi) {
		this.aqi = aqi;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public CurrentWeather getWeather() {
		return weather;
	}

	public void setWeather(CurrentWeather weather) {
		this.weather = weather;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public Integer getTemp() {
		return temp;
	}

	public void setTemp(Integer temp) {
		this.temp = temp;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public Float getElev_angle() {
		return elev_angle;
	}

	public void setElev_angle(Float elev_angle) {
		this.elev_angle = elev_angle;
	}

	public Float getApp_temp() {
		return app_temp;
	}

	public void setApp_temp(Float app_temp) {
		this.app_temp = app_temp;
	}
	
	
	
}