package apiEngine.model.response;


public class WeatherForecast extends WeatherBaseClass{

private Integer code;

/**
* No args constructor for use in serialization
*
*/
public WeatherForecast() {
}

/**
*
* @param code
* @param icon
* @param description
*/
public WeatherForecast(String icon, Integer code, String description) {
this.icon = icon;
this.code = code;
this.description = description;
}

public Integer getCode() {
	return code;
}

public void setCode(Integer code) {
	this.code = code;
}

public String getIcon() {
	return icon;
}

public void setIcon(String icon) {
	this.icon = icon;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

}
