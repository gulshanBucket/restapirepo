package apiEngine.model.response;

import java.util.List;

	public class ResponseCurrentWeatherData {
	
	private List<CurrentWeatherAndLocationData> data = null;
	private Integer count;
	
	/**
	* No args constructor for use in serialization
	*
	*/
	public ResponseCurrentWeatherData() {
	}
	
	/**
	*
	* @param data
	* @param count
	*/
	public ResponseCurrentWeatherData(List<CurrentWeatherAndLocationData> data, Integer count) {
	this.data = data;
	this.count = count;
	}

	public List<CurrentWeatherAndLocationData> getData() {
		return data;
	}

	public void setData(List<CurrentWeatherAndLocationData> data) {
		this.data = data;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
	
	
}
