package apiEngine;


import org.junit.Assert;

import apiEngine.model.response.ResponseCurrentWeatherData;
import apiEngine.model.response.ResponseWeatherForecast;
import dataProvider.ProjectDataClass;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class EndPoints {
	
	private ProjectDataClass dataClass = null;
	
	public EndPoints() {
		dataClass = ProjectDataClass.getInstance();
	}
	
	/**
	 * Method to get the current weather details
	 * @return ResponseCurrentWeatherData
	 */
	public ResponseCurrentWeatherData getCurrentWeatherDetail(){
		
		RequestSpecification request = getRequestForAPI();
              
        Response response = request.queryParam("lat", dataClass.getLatitude()).
        		queryParam("lon",dataClass.getLongitude()).
        		queryParam("key",dataClass.getKey()).get(Route.current());
       
        
        System.out.println(response.getBody().asString());
        System.out.println("Response Status: "+response.getStatusCode());
        
        Assert.assertEquals("Incorrect status code returned", response.getStatusCode(), 200);
        
        return response.getBody().as(ResponseCurrentWeatherData.class);
	}
	
	
	/**
	 * Method to get the weather forecast as per the postal code of the location
	 * @return ResponseWeatherForecast
	 */
	public ResponseWeatherForecast getWeatherForecast(){
		
        RequestSpecification request = getRequestForAPI();
              
        Response response = request.queryParam("postal_code", dataClass.getPostalCode()).
        		queryParam("country",dataClass.getCountryCode()).
        		queryParam("key",dataClass.getKey()).get(Route.forecast());
       
        
        System.out.println(response.getBody().asString());
        System.out.println("Response Status: "+response.getStatusCode());
        
        Assert.assertEquals("Incorrect status code returned", response.getStatusCode(), 200);
        
        return response.getBody().as(ResponseWeatherForecast.class);
	
		
	}
	
	/**
	 * Method to get the request as per the API URL
	 * @return RequestSpecification
	 * 
	 */

	public RequestSpecification getRequestForAPI(){
		RestAssured.baseURI = dataClass.getBaseURL();
        RequestSpecification request = RestAssured.given();
 
        request.header("Content-Type", "application/json");
        return request;
	}
}
