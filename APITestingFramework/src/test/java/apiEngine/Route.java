package apiEngine;

public class Route {
	
	private static final String CURRENT = "/current";
    private static final String POSTAL_CODE = "/forecast/hourly";
    private static final String VERSION = "/v2.0";
    
    
    public static String current(){ 
    	return VERSION + CURRENT;
    }
 
    
    public static String forecast(){ 
    	return VERSION + POSTAL_CODE;
    } 

}
